package com.example.petkeeper.model;

public class User {
    public String fullName;
    public String email;
    public String phone;

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String role;

    public User(){

    }

    public User(String fullName, String email, String phone){
        this.fullName = fullName;
        this.email = email;
        this.phone = phone;
        this.role = "korisnik";
    }
}
