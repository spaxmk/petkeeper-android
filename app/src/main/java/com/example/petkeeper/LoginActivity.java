package com.example.petkeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petkeeper.databinding.ActivityLoginBinding;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText mEmail,mPassword;
    Button btnLogin;
    ProgressBar progressBarLogin;

    TextView mRegisterBtn, forgotPassword;
    FirebaseAuth fAuth;

    private ActivityLoginBinding _binding;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _binding = ActivityLoginBinding.inflate(getLayoutInflater());
        View view = _binding.getRoot();
        setContentView(view);

        mEmail = _binding.txtEmail;
        mPassword = _binding.txtPassword;
        progressBarLogin = _binding.progressBarLogin;

        btnLogin = _binding.btnLogin;
        btnLogin.setOnClickListener(this);

        forgotPassword = _binding.lblForgotPassword;
        forgotPassword.setOnClickListener(this);

        mRegisterBtn = _binding.lblCreateText;
        mRegisterBtn.setOnClickListener(this);
        fAuth = FirebaseAuth.getInstance();



    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin:
                userLogin();
                break;
            case R.id.lblCreateText:
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                break;
            case R.id.lblForgotPassword:
                startActivity(new Intent(getApplicationContext(), ForgotPasswordActivity.class));
                break;
        }
    }

    private void userLogin() {
        String email = mEmail.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        progressBarLogin.setVisibility(View.VISIBLE);

        //Toast.makeText(Login.this, "Email: " + email + " Password " + password, Toast.LENGTH_LONG).show();

        if(email.isEmpty()){
            mEmail.setError("Polje je obavezno!");
            mEmail.requestFocus();
            progressBarLogin.setVisibility(View.GONE);
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            mEmail.setError("Unesite validan email");
            mEmail.requestFocus();
            progressBarLogin.setVisibility(View.GONE);
            return;
        }
        if(password.isEmpty()){
            mPassword.setError("Polje je obavezno!");
            mPassword.requestFocus();
            progressBarLogin.setVisibility(View.GONE);
            return;
        }
        if(password.length() < 6){
            mPassword.setError("Sifra mora da savrzi vise od 6 karaktera!");
            mPassword.requestFocus();
            progressBarLogin.setVisibility(View.GONE);
            return;
        }


        try {
            fAuth.signInWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                        if(user.isEmailVerified()){
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        }
                        else{
                            user.sendEmailVerification();
                            Toast.makeText(LoginActivity.this, "Email nije potvrdjen, proverite vase Email sanduce!", Toast.LENGTH_LONG).show();
                        }

                        progressBarLogin.setVisibility(View.GONE);
                    }else{
                        Toast.makeText(LoginActivity.this, "Neuspesno logovanje! \n Proverite Email i Sifru", Toast.LENGTH_LONG).show();
                        progressBarLogin.setVisibility(View.GONE);
                    }
                }
            });
        }
        catch (Exception e){
            Toast.makeText(LoginActivity.this, "Puko kec: " + e.toString(), Toast.LENGTH_LONG).show();
        }

    }
}