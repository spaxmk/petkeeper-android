package com.example.petkeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petkeeper.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    EditText mFullName, mEmail, mPassword, mPhone;
    Button btnRegistration;
    TextView mLoginBtn;
    ProgressBar progressBar;
    FirebaseAuth fAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mFullName = findViewById(R.id.txtFullName);
        mEmail = findViewById(R.id.txtEmail);
        mPassword = findViewById(R.id.txtPassword);
        mPhone = findViewById(R.id.txtPhone);
        btnRegistration = findViewById(R.id.btnRegister);
        mLoginBtn = findViewById(R.id.lblCreateText);

        fAuth = FirebaseAuth.getInstance();

        if(fAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(), MainActivity.class));
            finish();
        }

        progressBar = findViewById(R.id.progressBar);

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email = mEmail.getText().toString().trim();
                String password = mPassword.getText().toString().trim();
                String fullName = mFullName.getText().toString().trim();
                String phone = mPhone.getText().toString().trim();

                if(TextUtils.isEmpty(fullName)){
                    mFullName.setError("Polje je obavezno!");
                    mFullName.requestFocus();
                    return;
                }
                if(TextUtils.isEmpty(email)){
                    mEmail.setError("Polje Email je obavezno!");
                    mEmail.requestFocus();
                    return;
                }
                if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    mEmail.setError("Unesite ispravan Email!");
                    mEmail.requestFocus();
                    return;
                }
                if(TextUtils.isEmpty(password)){
                    mPassword.setError("Polje Sifra je obavezno!");
                    mPassword.requestFocus();
                    return;
                }
                if(password.length() < 6){
                    mPassword.setError("Sifra mora sadrzati vise od 6 karatkera");
                    mPassword.requestFocus();
                    return;
                }
                if(TextUtils.isEmpty(phone)){
                    mPhone.setError("Polje je obavezno!");
                    mPhone.requestFocus();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);

                fAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            User user = new User(fullName, email, phone);

                            FirebaseDatabase.getInstance().getReference("Users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(user)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()){
                                                Toast.makeText(RegisterActivity.this, "Uspesno ste napravili nalog!", Toast.LENGTH_LONG).show();

                                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

                                                if(user.isEmailVerified()){
                                                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                                }
                                                else{
                                                    user.sendEmailVerification();
                                                    Toast.makeText(RegisterActivity.this, "Email morate potvrditi, proverite vase Email sanduce!", Toast.LENGTH_LONG).show();
                                                    progressBar.setVisibility(View.GONE);
                                                    startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                                }
                                            }else{
                                                Toast.makeText(RegisterActivity.this, "Niste uspeli napraviti nalog!", Toast.LENGTH_LONG).show();
                                                progressBar.setVisibility(View.GONE);
                                            }

                                        }
                                    });
                        }else{
                            Toast.makeText(RegisterActivity.this, "Failed", Toast.LENGTH_LONG).show();
                            progressBar.setVisibility(View.GONE);
                        }
                    }
                });

            }
        });

        mLoginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            }
        });

    }
}