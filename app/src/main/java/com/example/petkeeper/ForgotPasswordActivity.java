package com.example.petkeeper;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ForgotPasswordActivity extends AppCompatActivity {

    private EditText txtEmail;
    private Button btnResetPassword;
    private ProgressBar progressBarForgotPass;

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        txtEmail = (EditText) findViewById(R.id.txtForgotPasswordEmail);
        btnResetPassword = (Button) findViewById(R.id.btnForgotPassword);
        progressBarForgotPass = (ProgressBar) findViewById(R.id.progressBarForgotPassword);

        auth = FirebaseAuth.getInstance();

        btnResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ResetPassword();
                //Toast.makeText(ForgotPasswordActivity.this, "Proverite vas Email za promenu sifre!", Toast.LENGTH_LONG).show();
            }
        });

    }

    private void ResetPassword(){
        String email = txtEmail.getText().toString().trim();

        if(email.isEmpty()){
            txtEmail.setError("Polje je obavezno!");
            txtEmail.requestFocus();
            return;
        }
        if(!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            txtEmail.setError("Unesite ispravnu Email adresu!");
            txtEmail.requestFocus();
            return;
        }

        progressBarForgotPass.setVisibility(View.VISIBLE);

        auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    progressBarForgotPass.setVisibility(View.GONE);
                    Toast.makeText(ForgotPasswordActivity.this, "Proverite vas Email za promenu sifre!", Toast.LENGTH_LONG).show();
                }
                else{
                    progressBarForgotPass.setVisibility(View.GONE);
                    Toast.makeText(ForgotPasswordActivity.this, "Pokusajte ponovo!", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}