package com.example.petkeeper.fragments;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.petkeeper.R;

public class AddNewViewHolder extends RecyclerView.ViewHolder {

    public TextView txtPetName;
    public TextView txtUserName;
    public TextView txtStatus;

    public AddNewViewHolder(@NonNull View itemView) {
        super(itemView);

        txtPetName = itemView.findViewById(R.id.txtPetName);
        txtUserName = itemView.findViewById(R.id.txtUserName);
        txtStatus = itemView.findViewById(R.id.txtStatus);

    }
}
