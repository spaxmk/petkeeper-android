package com.example.petkeeper.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.petkeeper.LoginActivity;
import com.example.petkeeper.databinding.FragmentProfileBinding;
import com.example.petkeeper.model.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ProfileFragment extends Fragment {
    public FirebaseUser user;
    private DatabaseReference reference;

    private FragmentProfileBinding _binding;

    private String userId;

    ProgressBar progressBarProfileFragment;
    private TextView lblProfileFragment;
    private TextView lblProfileName;
    private TextView tvFullName;
    private TextView tvEmail;
    private TextView tvPhone;
    private Button btnLogout;


    //public void onCreate(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    //public void onCreate(Bundle savedInstanceState) {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        _binding = FragmentProfileBinding.inflate(getLayoutInflater());
        View view = _binding.getRoot();

        return view;


    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        user = FirebaseAuth.getInstance().getCurrentUser();
        reference = FirebaseDatabase.getInstance().getReference("Users");
        userId = user.getUid();

        lblProfileName = _binding.lblProfileName;


        lblProfileFragment = _binding.lblProfileFragment;
        tvFullName = _binding.tvFullName;
        tvEmail = _binding.tvEmail;
        tvPhone = _binding.tvPhone;
        btnLogout = _binding.btnLogout;

        progressBarProfileFragment = _binding.progressBarProfileFragment;
        progressBarProfileFragment.setVisibility(View.VISIBLE);

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(getActivity(), LoginActivity.class));
            }
        });


        reference.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                User userProfile = snapshot.getValue(User.class);

                if (userProfile != null) {
                    String fullName = userProfile.fullName;
                    String email = userProfile.email;
                    String phone = userProfile.phone;

                    lblProfileName.setText(fullName);

                    tvFullName.setText(fullName);
                    tvEmail.setText(email);
                    tvPhone.setText(phone);
                    progressBarProfileFragment.setVisibility(View.GONE);
                } else {
                    Toast.makeText(getActivity(), "userProfile je null", Toast.LENGTH_LONG).show();
                    progressBarProfileFragment.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }

        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _binding = null;
    }
}
