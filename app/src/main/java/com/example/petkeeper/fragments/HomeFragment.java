package com.example.petkeeper.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.petkeeper.R;
import com.example.petkeeper.databinding.FragmentAddNewBinding;
import com.example.petkeeper.databinding.FragmentHomeBinding;
import com.example.petkeeper.model.AddNew;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class HomeFragment extends Fragment {

    private @NonNull FragmentHomeBinding _binding;

    private RecyclerView recyclerViewHome;
    private DatabaseReference reference;
    private FirebaseRecyclerOptions<AddNew> options;
    //private FirebaseRecyclerAdapter<AddNew,AddNewViewHolder> adapter;
    MyAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //return super.onCreateView(inflater, container, savedInstanceState);
        getActivity().getClass().getSimpleName();
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        recyclerViewHome = (RecyclerView) view.findViewById(R.id.recyclerViewHome);
        recyclerViewHome.setLayoutManager(new LinearLayoutManager(getContext()));

        FirebaseRecyclerOptions<AddNew> options = new FirebaseRecyclerOptions.Builder<AddNew>().setQuery(FirebaseDatabase.getInstance().getReference().child("Reservations"), AddNew.class).build();
        adapter = new MyAdapter(options);

        recyclerViewHome.setAdapter(adapter);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(recyclerViewHome);
/*
        _binding = FragmentHomeBinding.inflate(getLayoutInflater());
        View view = _binding.getRoot();

        reference = FirebaseDatabase.getInstance().getReference().child("Reservation");
        reference.keepSynced(true);
*/

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //super.onViewCreated(view, savedInstanceState);
        getActivity().getClass().getSimpleName();

    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
        @Override
        public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
            Toast.makeText(getActivity(), "Povukao si " + direction, Toast.LENGTH_SHORT);
        }
    };
}
