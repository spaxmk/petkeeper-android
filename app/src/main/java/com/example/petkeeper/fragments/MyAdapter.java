package com.example.petkeeper.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.DeadObjectException;
import android.telephony.ims.ImsMmTelManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.GenericLifecycleObserver;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.petkeeper.R;
import com.example.petkeeper.databinding.FragmentDetailsBinding;
import com.example.petkeeper.model.AddNew;
import com.example.petkeeper.model.User;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.dynamic.IFragmentWrapper;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.regex.Pattern;

public class MyAdapter extends FirebaseRecyclerAdapter<AddNew, MyAdapter.myviewholder> {

    public MyAdapter(@NonNull FirebaseRecyclerOptions<AddNew> options) {
        super(options);
    }

    public FirebaseUser user;
    private DatabaseReference referenceUsers;
    private String userId;
    private String loggedUserRole;
    private String roleAdmin = "admin";
    private String keyPosition;
    private String odbijenaRezervacija = "Odbijena rezervacija";
    private String prihvacenaRezervacija = "Prihvacena rezervacija";

    @Override
    protected void onBindViewHolder(@NonNull myviewholder holder, int position, @NonNull AddNew model) {
        holder.txtPetName.setText(model.Name);
        holder.txtUserName.setText(model.UserFullName);
        holder.txtStatus.setText(model.Status);

        if(model.Status.equals(prihvacenaRezervacija)){
            holder.txtStatus.setTextColor(Color.parseColor("#30CB0B"));
        }
        if(model.Status.equals(odbijenaRezervacija)){
            holder.txtStatus.setTextColor(Color.RED);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatActivity activity = (AppCompatActivity) v.getContext();

                keyPosition = FirebaseDatabase.getInstance().getReference().child("Reservations").child(getRef(position).getKey()).getKey();

                DetailsFragment detailsFragment = new DetailsFragment(model.Name, model.Status, model.DogBreed, model.Gender, model.DateCheckIn, model.DateCheckOut, model.UserFullName, model.Age, keyPosition);
                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_layout, detailsFragment).addToBackStack(null).commit();

            }
        });


/*
        user = FirebaseAuth.getInstance().getCurrentUser();
        referenceUsers = FirebaseDatabase.getInstance().getReference("Users");
        userId = user.getUid();

        try {
            referenceUsers.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User userProfile = snapshot.getValue(User.class);

                    if(userProfile != null){
                        loggedUserRole = userProfile.role;

                        if(loggedUserRole.equals(roleAdmin)){
                            new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
                                @Override
                                public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder target) {
                                    return false;
                                }

                                @Override
                                public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int direction) {
                                    holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                                        @Override
                                        public boolean onLongClick(View v) {
                                            AppCompatActivity activity = (AppCompatActivity) v.getContext();
                                            Toast.makeText(activity, "Jako si drzao, probices ekran! Pusti bree", Toast.LENGTH_SHORT).show();

                                            FirebaseDatabase.getInstance().getReference().child("Reservations").child(getRef(position).getKey()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    Toast.makeText(activity, "Item je obrisan! Pusti bree", Toast.LENGTH_SHORT).show();
                                                }
                                            });

                                            return true;
                                        }
                                    });
                                }
                            }).attachToRecyclerView(null);

                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        catch (Exception ex){

        }

*/


        user = FirebaseAuth.getInstance().getCurrentUser();
        referenceUsers = FirebaseDatabase.getInstance().getReference("Users");
        userId = user.getUid();

        try {
            referenceUsers.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User userProfile = snapshot.getValue(User.class);

                    if(userProfile != null){
                        loggedUserRole = userProfile.role;

                        if(loggedUserRole.equals(roleAdmin)){

                            holder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {
                                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                                    Toast.makeText(activity, "Jako si drzao, probices ekran! Pusti bree", Toast.LENGTH_SHORT).show();

                                    FirebaseDatabase.getInstance().getReference().child("Reservations").child(getRef(position).getKey()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(activity, "Rezervacija je obrisana! Pusti bree", Toast.LENGTH_SHORT).show();
                                        }
                                    });

                                    return true;
                                }
                            });
                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        catch (Exception ex){

        }
    }

    @NonNull
    @Override
    public myviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_item , parent, false);

        user = FirebaseAuth.getInstance().getCurrentUser();
        referenceUsers = FirebaseDatabase.getInstance().getReference("Users");
        userId = user.getUid();


        return new myviewholder(view);
    }



    public class myviewholder extends RecyclerView.ViewHolder{

        public TextView txtPetName;
        public TextView txtUserName;
        public TextView txtStatus;

        public myviewholder(@NonNull View itemView) {
            super(itemView);

            txtPetName = itemView.findViewById(R.id.txtPetName);
            txtUserName = itemView.findViewById(R.id.txtUserName);
            txtStatus = itemView.findViewById(R.id.txtStatus);

        }
    }
}
