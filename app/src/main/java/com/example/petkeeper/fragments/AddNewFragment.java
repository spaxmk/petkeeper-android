package com.example.petkeeper.fragments;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petkeeper.MainActivity;
import com.example.petkeeper.R;
import com.example.petkeeper.databinding.FragmentAddNewBinding;
import com.example.petkeeper.model.AddNew;
import com.example.petkeeper.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.Calendar;
import java.util.Date;


public class AddNewFragment extends Fragment implements AdapterView.OnItemSelectedListener {

    private @NonNull FragmentAddNewBinding _binding;

    private AddNew newReservation;

    public FirebaseUser user;
    private DatabaseReference reference;
    private DatabaseReference referenceUsers;

    private Spinner spinnerDogBreed;
    private Spinner spinnerGender;

    private DatePickerDialog.OnDateSetListener DateCheckInSetListener;
    private DatePickerDialog.OnDateSetListener DateCheckOutSetListener;

    private String userId;
    private String name;
    private int age = 4;
    private String dogBreed;
    private String gender;
    private String checkInDate;
    private String checkOutDate;
    private String userFullName;
    private String userEmail;

    private TextView tvCheckInDate;
    private TextView tvCheckOutDate;

    private Button btnAddReservation;

/*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
*/

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        _binding = FragmentAddNewBinding.inflate(getLayoutInflater());
        View view = _binding.getRoot();

        return view;
        //return inflater.inflate(R.layout.fragment_add_new, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        reference = FirebaseDatabase.getInstance().getReference().child("Reservations");
        user = FirebaseAuth.getInstance().getCurrentUser();
        referenceUsers = FirebaseDatabase.getInstance().getReference("Users");
        userId = user.getUid();

        //Date Binding
        spinnerDogBreed = _binding.spinnerDogBreed;
        spinnerGender = _binding.spinnerGender;

        tvCheckOutDate = _binding.tvCheckOutDate;
        tvCheckInDate = _binding.tvCheckInDate;

        name = _binding.txtName.getText().toString().trim();

        btnAddReservation = _binding.btnAddReservation;


        //Spinner DogBreed
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(), R.array.DogBreed, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerDogBreed.setAdapter(adapter);
        spinnerDogBreed.setOnItemSelectedListener(this);
        dogBreed = spinnerDogBreed.getSelectedItem().toString();

        //Spinner Gender
        ArrayAdapter<CharSequence> adapterGender = ArrayAdapter.createFromResource(getActivity(), R.array.Gender, android.R.layout.simple_spinner_item);
        adapterGender.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerGender.setAdapter(adapterGender);
        spinnerGender.setOnItemSelectedListener(this);
        gender = spinnerGender.getSelectedItem().toString();

        //Check-in Date
        tvCheckInDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialogCheckIn = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Dialog_MinWidth, DateCheckInSetListener, year,month,day );
                dialogCheckIn.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogCheckIn.show();
            }
        });

        DateCheckInSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int yearP, int monthP, int dayOfMonthP) {
                monthP += 1;
                checkInDate = dayOfMonthP + "/" + monthP + "/" + yearP;
                tvCheckInDate.setText("Datum prijema: " + checkInDate);
            }
        };

        //Check-out Date
        tvCheckOutDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar cal2 = Calendar.getInstance();
                int year = cal2.get(Calendar.YEAR);
                int month = cal2.get(Calendar.MONTH);
                int day = cal2.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialogCheckOut = new DatePickerDialog(getActivity(), android.R.style.Theme_Holo_Dialog_MinWidth, DateCheckOutSetListener, year,month,day );
                dialogCheckOut.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialogCheckOut.show();
            }
        });

        //Adding new Reservation
        try {
            referenceUsers.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User userProfile = snapshot.getValue(User.class);

                    if(userProfile != null){
                        userFullName = userProfile.fullName;
                        userEmail = userProfile.email;
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        catch (Exception ex){
            Toast.makeText(getActivity(), "Puko kec kod user-a \n" + ex.toString(), Toast.LENGTH_LONG).show();
        }

        btnAddReservation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    newReservation = new AddNew();
                    name = _binding.txtName.getText().toString();
                    String ageString = _binding.txtAge.getText().toString().trim();
                    if(!ageString.isEmpty()){
                        age = Integer.parseInt(ageString);
                    }
                    dogBreed = _binding.spinnerDogBreed.getSelectedItem().toString();
                    gender = _binding.spinnerGender.getSelectedItem().toString();



                    if(!name.isEmpty() && !dogBreed.isEmpty() && !gender.isEmpty() && !checkInDate.isEmpty() && !checkOutDate.isEmpty()){
                        newReservation.Name = name;
                        newReservation.Age = age;
                        newReservation.Gender = gender;
                        newReservation.DogBreed = dogBreed;
                        newReservation.DateCheckIn = checkInDate;
                        newReservation.DateCheckOut = checkOutDate;
                        newReservation.Status = "Na cekanju";
                        newReservation.UserFullName = userFullName;
                        newReservation.UserEmail = userEmail;

                        reference.push().setValue(newReservation).addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                AppCompatActivity activity = (AppCompatActivity) getContext();
                                activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_layout , new HomeFragment()).addToBackStack(null).commit();
                            }
                        });

                        Toast.makeText(getActivity(), "Uspesno ste dodali rezervaciju!", Toast.LENGTH_LONG).show();

                    }
                    else{
                        Toast.makeText(getActivity(), "Sva polja su obavezna!", Toast.LENGTH_LONG).show();
                    }
                }
                catch (Exception ex2){
                    Toast.makeText(getActivity(), "Puko kec kod dodavanja \n" + ex2.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });


        DateCheckOutSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int yearO, int monthO, int dayOfMonthO) {
                monthO += 1;
                checkOutDate = dayOfMonthO + "/" + monthO + "/" + yearO;
                tvCheckOutDate.setText("Datum odjave: " + checkOutDate);
            }
        };

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        //Toast.makeText(getActivity(), "Izabrano je: " + parent.getItemAtPosition(position).toString(), Toast.LENGTH_LONG).show();

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

}