package com.example.petkeeper.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.petkeeper.R;
import com.example.petkeeper.databinding.FragmentDetailsBinding;
import com.example.petkeeper.databinding.FragmentHomeBinding;
import com.example.petkeeper.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

public class DetailsFragment extends Fragment {

    private @NonNull FragmentDetailsBinding _binding;

    String keyPosition;
    String name;
    String status;
    String dogBreed;
    String gender;
    String dateCheckIn;
    String dateCheckOut;
    String userFullName;
    String loggedUser;
    String logedUserEmail;
    String loggedUserRole;
    int age;

    String roleAdmin = "admin";

    private Button buttonPrihvati;
    private Button buttonOdbaci;

    public FirebaseUser user;
    private DatabaseReference reference;
    private DatabaseReference referenceUsers;
    private String userId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        _binding = FragmentDetailsBinding.inflate(getLayoutInflater());
        View view = _binding.getRoot();

        TextView txtNameDetails = _binding.txtNameDetails;
        TextView txtAgeDetails = _binding.txtAgeDetails;
        TextView txtGenderDetails = _binding.txtGenderDetails;
        TextView txtDogBreedDetails = _binding.txtDogBreedDetails;
        TextView txtDateCheckInDetails = _binding.txtDateCheckInDetails;
        TextView txtDeteCheckOutDetails = _binding.txtDateCheckOutDetails;
        TextView txtStatusDetails = _binding.txtStatusDetails;
        TextView txtUserFullName = _binding.txtUserFullName;
        TextView txtPetName = _binding.lblPetName;

        txtNameDetails.setText(name);
        txtPetName.setText(name);
        txtStatusDetails.setText(status);
        txtAgeDetails.setText(String.valueOf(age));
        txtDateCheckInDetails.setText(dateCheckIn);
        txtDeteCheckOutDetails.setText(dateCheckOut);
        txtGenderDetails.setText(gender);
        txtDogBreedDetails.setText(dogBreed);
        txtUserFullName.setText(userFullName);

        buttonPrihvati = _binding.btnPrihvati;
        buttonOdbaci = _binding.buttonOdbaci;

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        reference = FirebaseDatabase.getInstance().getReference().child("Reservations");
        user = FirebaseAuth.getInstance().getCurrentUser();
        referenceUsers = FirebaseDatabase.getInstance().getReference("Users");
        userId = user.getUid();

        try {
            referenceUsers.child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User userProfile = snapshot.getValue(User.class);

                    if(userProfile != null){
                        loggedUser = userProfile.fullName;
                        logedUserEmail = userProfile.email;
                        loggedUserRole = userProfile.role;

                        if(loggedUserRole.equals(roleAdmin)){
                            buttonOdbaci.setVisibility(View.VISIBLE);
                            buttonPrihvati.setVisibility(View.VISIBLE);
                        }
                    }

                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
        }
        catch (Exception ex){
            Toast.makeText(getActivity(), "Puko kec kod user-a \n" + ex.toString(), Toast.LENGTH_LONG).show();
        }

        buttonPrihvati.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String updateStatus = "Prihvacena rezervacija";

                Map<String,Object> map = new HashMap<>();
                map.put("Age", age);
                map.put("DateCheckIn", dateCheckIn);
                map.put("DateCheckOut", dateCheckOut);
                map.put("DogBreed", dogBreed);
                map.put("Gender", gender);
                map.put("Name", name);
                map.put("Status", updateStatus);

                reference.child(keyPosition).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        AppCompatActivity activity = (AppCompatActivity) getContext();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_layout , new HomeFragment()).addToBackStack(null).commit();
                        Toast.makeText(getContext(), "Uspesno ste promenuli status!", Toast.LENGTH_SHORT);
                    }
                });
            }
        });

        buttonOdbaci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String updateStatus = "Odbijena rezervacija";

                Map<String,Object> map = new HashMap<>();
                map.put("Age", age);
                map.put("DateCheckIn", dateCheckIn);
                map.put("DateCheckOut", dateCheckOut);
                map.put("DogBreed", dogBreed);
                map.put("Gender", gender);
                map.put("Name", name);
                map.put("Status", updateStatus);

                reference.child(keyPosition).updateChildren(map).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        Toast.makeText(getContext(), "Uspesno ste promenuli status!", Toast.LENGTH_SHORT);
                        AppCompatActivity activity = (AppCompatActivity) getContext();
                        activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment_layout , new HomeFragment()).addToBackStack(null).commit();
                    }
                });
            }
        });
    }

    public DetailsFragment(String Name, String Status, String DogBreed, String Gender, String DateCheckIn, String DateCheckOut, String UserFullName, int Age, String KeyPosition ){
        this.name = Name;
        this.status = Status;
        this.dogBreed = DogBreed;
        this.gender = Gender;
        this.dateCheckIn = DateCheckIn;
        this.dateCheckOut = DateCheckOut;
        this.userFullName = UserFullName;
        this.age = Age;
        this.keyPosition = KeyPosition;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        _binding = null;
    }
}